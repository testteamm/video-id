package eu.electronicid.tutorial;

import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tuckey.web.filters.urlrewrite.UrlRewriteFilter;

import javax.servlet.DispatcherType;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;

/**
 * Created by Tania on 12/04/2016.
 */

@Configuration
public class Config
{

    @Bean
    FilterRegistrationBean filterRegistrationBean() throws URISyntaxException
    {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();

        registrationBean.setFilter(new UrlRewriteFilter());
        registrationBean.addUrlPatterns("/*");
        registrationBean.setDispatcherTypes(DispatcherType.FORWARD, DispatcherType.REQUEST);

        registrationBean.addInitParameter("confPath", "urlrewrite.xml");

        return registrationBean;
    }
}

