package eu.electronicid.tutorial;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Tania on 12/04/2016.
 */
@Controller
public class VideoIdController
{
    @RequestMapping("/")
    public String index () {
        return "index";
    }

    @RequestMapping("/video-id")
    public String videoId () {
        return "videoId";
    }
}
